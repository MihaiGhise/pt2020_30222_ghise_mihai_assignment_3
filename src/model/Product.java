package model;
/**
 * 
 * @author Mihai
 *	Clasa Product
 */
public class Product {
	
	private int idProduct;
	private String productName;
	private int stock;
	private double price;
	
	public Product() {
		super();
	}
	
	public Product(int id, String productName, int stock, double price) {
		super();
		this.idProduct=id;
		this.productName=productName;
		this.stock=stock;
		this.price = price;
		
	}
	
	public Product(String productName, int stock, double price) {
		super();
		this.productName=productName;
		this.stock=stock;
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String toString() {
		return "Product [id=" + idProduct + ", name=" + productName + "]";
	}

}
