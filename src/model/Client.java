package model;

public class Client {
	
	private int idClient;
	private String name;
	private String city;
	
	public Client() {
		super();
	}
	
	public Client(int id, String name, String city) {
		super();
		this.idClient=id;
		this.name=name;
		this.city=city;
	}
	
	public Client(String name, String city) {
		super();
		this.name=name;
		this.city=city;
	}
	
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String toString() {
		return "Client [id=" + idClient + ", name=" + name + ", city" + city + "]";
	}

}
