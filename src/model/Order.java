package model;

public class Order {
	
	private int idOrder;
	private String clientName;
	private String productName;
	private int quantity;
	private double productPrice;
	
	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public Order() {
		super();
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Order(int id, String clientName, String productName, int quantity, double productPrice) {
		super();
		this.idOrder=id;
		this.clientName=clientName;
		this.productName=productName;
		this.quantity=quantity;
		this.productPrice=productPrice;
	}
	
	public Order(String clientName, String productName, int quantity, double productPrice) {
		super();
		this.clientName=clientName;
		this.productName=productName;
		this.quantity=quantity;
		this.productPrice=productPrice;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	
	public String toString() {
		return "Order [id=" + idOrder  + "]";
	}

}
