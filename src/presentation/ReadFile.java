package presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

/**
* This is the class where data is introduced and pdf files are generated
* It gets the file name with the input data as constructor argument
*/
public class ReadFile {
	
	private Scanner f;
	private List<String> productsNames = new ArrayList<String>();
	private List<Product> products = new ArrayList<Product>();
	private List<Client> clients = new ArrayList<Client>();
	private List<Order> orders = new ArrayList<Order>();
	private Document document;
	private int raportNb = 0;
	private int orderNb = 0;
	
	/**
	 * Here we create a scanner object which will read from the file
	 */
	public ReadFile(String fileName) {
		try {
			f = new Scanner(new File(fileName));
		}catch(Exception e){
			System.out.println("File not found");
		}
		readFile();
		f.close();
	}
	
	/**
	 * The program reads from the file
	 */
	private void readFile() {
		String a=null, b=null, c=null, d = null, e = null;
		while(f.hasNext()) {
			 a = f.next();
			 if (a.equals("Report")) {
				 b = f.next();
			 }
			 else {
			 b = f.next();
			 	if(a.equals("Delete") && b.equals("Product:")) {
			 		c = f.next();
			 	}else {
			 c = f.next();
			 d = f.next();
			 e = f.next();
			 	}
			 	}
			 c=separate(c);
			 d=separate(d);
			 dataEntry(a, b, c, d, e);
		}	
	}
	
	private void insert(String a, String b, String c, String d, String e) {
		if(b.equals("client:")) {
			Client client = new Client(c+" "+d, e);
			ClientDAO clientDAO = new ClientDAO();
			clientDAO.insert(client);
		}else if(b.equals("product:")) {
			Product product = new Product(c, Integer.parseInt(d),  Double.parseDouble(e));
			ProductDAO productDAO = new ProductDAO();
			if(productsNames.contains(product.getProductName())) {
				productDAO.updateStock(product);
				System.out.println("contine");
			}else {
				productsNames.add(product.getProductName());
				productDAO.insert(product);
			}
		}
	}
	
	private void delete(String a, String b, String c, String d, String e) {
		if(b.equals("client:")) {
			Client client = new Client(c+" "+d, e);
			ClientDAO clientDAO = new ClientDAO();
			clientDAO.delete(client);
		}else if(b.equals("Product:")) {
			Product product = new Product(c, Integer.parseInt(d),  Double.parseDouble(e));
			ProductDAO productDAO = new ProductDAO();
				productDAO.delete(product);
		}
	}
	
	private void orderBill(String a, String b, String c, String d, String e) {
		Order order = new Order(b+" "+c, d, Integer.parseInt(e), 0.0);
		ProductDAO productsDAO = new ProductDAO();
		products = productsDAO.findAll("products");
		for(Product p : products) {
			if(p.getProductName().equals(d)) {
				order.setProductPrice(p.getPrice());
			}}
		OrderDAO orderDAO = new OrderDAO();
		orderDAO.insert(order);
		createBillOrders(order);
	}
	
	private void report(String a, String b) {
		if(b.equals("client")) {
			ClientDAO clientDAO = new ClientDAO();
			clients = clientDAO.findAll("clients");
			createDocumentClients(clients);
			clients.forEach((n) -> System.out.println(n.getName()));
		}else if(b.equals("product")) {
			ProductDAO productDAO = new ProductDAO();
			products = productDAO.findAll("products");
			createDocumentProducts(products);
		}else if(b.equals("order")) {
			OrderDAO orderDAO = new OrderDAO();
			orders = orderDAO.findAll("orders");
			createDocumentOrders(orders);
		}
	}
	
	/**
	 * This method allows manages the input and classifies it 
	 * by the command and the object type, using the other methods to create objects coresponding to the input
	 */
	private void dataEntry(String a, String b, String c, String d, String e) {
		if(a.equals("Insert")) {
			insert(a, b, c, d, e);
		}else if(a.equals("Delete")) {
			delete(a, b, c, d, e);
		}
		else if(a.equals("Order:")) {
			orderBill(a, b, c, d, e);
		}else if(a.equals("Report")) {
			report(a, b);
		}
	}
	
	/**
	 * Generates pdf documents for clients
	 */
	private void createDocumentClients(List<Client> list) {
		document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Report"+raportNb+ ".pdf"));
			document.open();
			PdfPTable table = new PdfPTable(3);
			PdfPCell c1 = new PdfPCell(new Paragraph("id"));
			PdfPCell c2 = new PdfPCell(new Paragraph("name"));
			PdfPCell c3 = new PdfPCell(new Paragraph("city"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			for(Client c : list) {
				table.addCell(String.valueOf(c.getIdClient()));
				table.addCell(c.getName());
				table.addCell(c.getCity());
			}
			document.add(table);
			document.close();
		}catch(DocumentException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			raportNb++;
		}
	}
	
	/**
	 * Generates pdf documents for products
	 */
	private void createDocumentProducts(List<Product> list) {
		document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Report"+raportNb+ ".pdf"));
			document.open();
			PdfPTable table = new PdfPTable(4);
			PdfPCell c1 = new PdfPCell(new Paragraph("id"));
			PdfPCell c2 = new PdfPCell(new Paragraph("name"));
			PdfPCell c3 = new PdfPCell(new Paragraph("stock"));
			PdfPCell c4 = new PdfPCell(new Paragraph("price"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
			for(Product c : list) {
				table.addCell(String.valueOf(c.getIdProduct()));
				table.addCell(c.getProductName());
				table.addCell(String.valueOf(c.getStock()));
				table.addCell(String.valueOf(c.getPrice()));
			}
			document.add(table);
			document.close();
		}catch(DocumentException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			raportNb++;
		}
	}
	
	/**
	 * Generates pdf documents for orders
	 */
	private void createDocumentOrders(List<Order> list) {
		document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Report"+raportNb+ ".pdf"));
			document.open();
			PdfPTable table = new PdfPTable(4);
			PdfPCell c1 = new PdfPCell(new Paragraph("id"));
			PdfPCell c2 = new PdfPCell(new Paragraph("clientName"));
			PdfPCell c3 = new PdfPCell(new Paragraph("productName"));
			PdfPCell c4 = new PdfPCell(new Paragraph("quantity"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
			for(Order c : list) {
				table.addCell(String.valueOf(c.getIdOrder()));
				table.addCell(c.getClientName());
				table.addCell(String.valueOf(c.getProductName()));
				table.addCell(String.valueOf(c.getQuantity()));
			}
			document.add(table);
			document.close();
		}catch(DocumentException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			raportNb++;
		}
	}
	
	private void createBillOrders(Order order) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Bill"+orderNb+".pdf"));
			document.open();
			PdfPTable table = new PdfPTable(4);
			PdfPCell c1 = new PdfPCell(new Paragraph("clientName"));
			PdfPCell c2 = new PdfPCell(new Paragraph("productName"));
			PdfPCell c3 = new PdfPCell(new Paragraph("quantity"));
			PdfPCell c4 = new PdfPCell(new Paragraph("totalPrice"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
				table.addCell(order.getClientName());
				table.addCell(order.getProductName());
				table.addCell(String.valueOf(order.getQuantity()));
				table.addCell(String.valueOf(order.getQuantity()*order.getProductPrice()));
			document.add(table);
			document.close();
		}catch(DocumentException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			orderNb++;
		}
	}

	/** 
	 * This method separates the comma from the rest of the String
	 */
	private String separate(String word) {
	     String retWord = new String();
		 Pattern polyFormat = Pattern.compile("\\w+");
	     Matcher m = polyFormat.matcher(word);
	     while (m.find()){
				if (m.group().length() != 0){
						retWord=m.group().trim();
				
	}}
	     return retWord;
	}
}
