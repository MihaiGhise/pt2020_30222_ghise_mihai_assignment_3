package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * 
 * @author Mihai
 *	Clasa pentru a stabili conexiunea cu baza de date
 */
public class ConnectionFactory {
	
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/schemanoua";
	private static final String USER = "root";
	private static final String PASS = "mihai";
	
	private static ConnectionFactory singleInstance = new ConnectionFactory();
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metoda pentru a creea conexiunea
	 * @return conexiune
	 */
	private Connection createConnection() {
		Connection connection = null;
		System.out.println("a intrat in creare");
		//System.out.println(DBURL+" "+ USER+" "+ PASS);
		try {
			connection = DriverManager.getConnection(DBURL, USER, PASS);
			System.out.println("conectat cu succes");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			//e.printStackTrace();
		}
		return connection;
	}

	/**
	 * Metoda pentru a apela conexiunea creeata
	 * @return
	 */
	public static Connection getConnection() {
		return singleInstance.createConnection();
	}
	
	/**
	 * Metoda pentru inchiderea conexiunii
	 * @param connection
	 */
	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
	}
	
	/**
	 * Metoda pentru inchiderea statement-ului
	 * @param statement
	 */
	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
			}
		}
	}

	/**
	 * Metoda pentru inchiderea resultSet-ului
	 * @param resultSet
	 */
	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}

}
