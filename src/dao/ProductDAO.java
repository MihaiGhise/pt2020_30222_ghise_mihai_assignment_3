package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import connection.ConnectionFactory;
import model.Client;
import model.Product;
/**
 * 
 * @author Mihai
 *	Clasa pentru a gestiona tabela products din baza de date
 */
public class ProductDAO extends AbstractDAO<Product> {
	
	/**
	 * Metoda pentru a insera in tabela products
	 */
	public Product insert(Product product) {
		PreparedStatement statement = null;
		int resultSet = 0;
		Connection connection =  null;
/*		connection = ConnectionFactory.getConnection();*/
		String query1 = "INSERT INTO products (productName, price, stock) values('"+ product.getProductName() +"', '" + product.getPrice() + "','"+ product.getStock()+ "')";
/*		String query2 = "UPDATE products SET stock = stock + "+ product.getStock()+ " WHERE productName='"+product.getProductName()+"';";
*/
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query1);
			resultSet = statement.executeUpdate();
//			statement = connection.prepareStatement(query2);
//			resultSet = statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
		//ConnectionFactory.close(resultSet);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
	}
		return product;
	}
	
	
	/**
	 * Metoda pentru a face update la stocul din tabela products
	 */
	public Product updateStock(Product product) {
		PreparedStatement statement = null;
		int resultSet = 0;
		Connection connection =  null;
		/*connection = ConnectionFactory.getConnection();*/
		String query = "UPDATE products SET stock = stock + "+ product.getStock()+ " WHERE productName='"+product.getProductName()+"';";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
		//ConnectionFactory.close(resultSet);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
	}
		return product;
	}
	
	/**
	 * Metoda pentru a sterge din tabela products
	 */
	public Product delete(Product product) {
		Connection connection =  null;
		int resultSet = 0;
		/*connection = ConnectionFactory.getConnection();*/
		String query = "DELETE FROM products WHERE productName = '" + product.getProductName() + "';";	
		PreparedStatement statement=null;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//ConnectionFactory.close(resultSet);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
		
		return product;
	}


}
