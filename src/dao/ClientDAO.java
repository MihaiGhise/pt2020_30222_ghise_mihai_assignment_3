package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;

import connection.ConnectionFactory;
import model.Client;
/**
 * 
 * @author Mihai
 *	Clasa pentru a gestiona tabela clients din baza de date
 */
public class ClientDAO extends AbstractDAO<Client> {
	
	/**
	 * Metoda pentru a insera in tabela clients
	 */
	public Client insert(Client client) {
		PreparedStatement statement = null;
		int resultSet = 0;
		Connection connection =  null;
		String query = "INSERT INTO clients(name, city) values('"+ client.getName() + "', '"+client.getCity() + "');";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			//ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return client;
	}
	
	/** 
	 * Metoda pentru a sterge din tabela clients
	 */
	public Client delete(Client client) {
		Connection connection =  null;
		int resultSet = 0;
		connection = ConnectionFactory.getConnection();
		String query = "DELETE FROM clients WHERE name = '" + client.getName() + "';";	
		PreparedStatement statement=null;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//ConnectionFactory.close(resultSet);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
		
		return client;
	}
	
}

