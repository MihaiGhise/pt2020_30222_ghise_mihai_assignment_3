package dao;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import connection.ConnectionFactory;
import model.Client;
import model.Order;
/**
 * 
 * @author Mihai
 *	Clasa pentru a gestiona tabela orders din baza de date
 */
public class OrderDAO extends AbstractDAO<Order> {
	private int orderNb=0;
	
	/**
	 * Metoda pentru a insera in tabela orders
	 */
	public Order insert(Order order) {
		Connection connection =  null;
		int resultSet = 0;
		connection = ConnectionFactory.getConnection();
		String query = "INSERT INTO orders (clientName, productName, quantity) values('"+ order.getClientName() +"', '" + order.getProductName() +"'," +order.getQuantity()+ ");";
		String query2 = "UPDATE products SET stock = stock - "+ order.getQuantity()+ " WHERE productName='"+order.getProductName()+"';";
		PreparedStatement statement=null;
		if(check(order) == true) {
			System.out.println(check(order));
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeUpdate();
			statement = connection.prepareStatement(query2);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e);
		}}else{
			System.out.println("Stoc insuficient");
		//ConnectionFactory.close(resultSet);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
	}
		return order;
	}
	
	/**
	 * Metoda pentru a verifica daca se poate insera in tabela orders
	 */
	private boolean check(Order order) {
		
		String query = "SELECT * FROM products WHERE productName = '"+order.getProductName()+"';";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
		} catch (SQLException e) {
			e.getMessage();
		} 	
		int stock=0;
			try {
				resultSet.next();
				stock=resultSet.getInt("stock");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		if(stock<order.getQuantity()) {
			return false;
			}else {
		return true;}
	}
	
	/**
	 * Metoda pentru creearea notei
	 */
	private void createBillOrders(Order order) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Bill"+orderNb+".pdf"));
			document.open();
			PdfPTable table = new PdfPTable(4);
			PdfPCell c1 = new PdfPCell(new Paragraph("clientName"));
			PdfPCell c2 = new PdfPCell(new Paragraph("productName"));
			PdfPCell c3 = new PdfPCell(new Paragraph("quantity"));
			PdfPCell c4 = new PdfPCell(new Paragraph("totalPrice"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
				table.addCell(order.getClientName());
				table.addCell(order.getProductName());
				table.addCell(String.valueOf(order.getQuantity()));
				table.addCell(String.valueOf(order.getQuantity()*order.getProductPrice()));
			document.add(table);
			document.close();
		}catch(DocumentException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			orderNb++;
		}
	}
		

}
